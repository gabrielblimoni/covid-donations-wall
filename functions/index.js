const getWallDonations = require('./utils/getWallDonations')
const getStorageItem = require('./utils/getStorageItem')
const setStorageItem = require('./utils/setStorageItem')
const runDonationsMerge = require('./utils/runDonationsMerge')

exports.generateDonationWall = async function (event, context) {
    // get pending donations
    const pending_wall_donations = await getWallDonations('pending')
    if (pending_wall_donations.length === 0) {
        console.log('No donations to execute')
        return true
    }
    // get done donations
    const done_wall_donations = await getWallDonations('done')

    const wall_image_name = 'wall.png'
    // downloads wall
    await getStorageItem('wall', wall_image_name)
    // backups wall
    await setStorageItem(wall_image_name, `backups/${Date.now()}`)
    // merges wall
    await runDonationsMerge(wall_image_name, pending_wall_donations, done_wall_donations)
    // uploads wall
    await setStorageItem(wall_image_name, 'wall')

    return true
}
