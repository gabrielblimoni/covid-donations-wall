const {Storage} = require('@google-cloud/storage')
const storage = new Storage()
const bucket = 'donations-covid-england.appspot.com'

module.exports = async function (filename, ref) {
  const file_path = `/tmp/${filename}`
  await storage.bucket(bucket).upload(file_path, {
    gzip: true,
    destination: ref,
    metadata: {
      cacheControl: 'no-cache',
    }
  })
}