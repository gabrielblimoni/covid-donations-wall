const {Firestore} = require('@google-cloud/firestore')
const firestore = new Firestore()

module.exports = async function (status) {
  const snapshot = await firestore
    .collection('wall_donations')
    .where('status', '==', status)
    .get()
  
  const donations = snapshot.docs.map(doc => {
    const result = doc.data()
    result.id = doc.id
    return result
  })

  return donations
}