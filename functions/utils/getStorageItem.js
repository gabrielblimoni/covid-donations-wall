const {Storage} = require('@google-cloud/storage')
const storage = new Storage()
const bucket = 'donations-covid-england.appspot.com'

module.exports = async function (ref, file_name) {
  const destination = `/tmp/${file_name}`
  const options = {
    destination
  }  

  await storage.bucket(bucket).file(ref).download(options)
}