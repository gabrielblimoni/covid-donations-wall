const {Firestore} = require('@google-cloud/firestore')
const firestore = new Firestore()

module.exports = async function (doc_id, donation_map) {
  await firestore.collection('wall_donations').doc(String(doc_id)).update({ donation_map, status: 'done' })
}