module.exports = function () {
  let config = {}
  config.max_width = 1000
  config.max_height = 1000
  config.donations_map = []
  return config
}