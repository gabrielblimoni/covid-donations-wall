const buildConfig = require('./buildConfig')
const getStorageItem = require('../getStorageItem')
const mergeDonationImage = require('./mergeDonationImage')
const setDonationMap = require('./setDonationMap')

module.exports = async function (wall_img_name, pending_donations, done_donations) {
  const config = await buildConfig(done_donations)

  for (const donation of pending_donations) {
    // download donations img
    await getStorageItem(`donations/${donation.id}`, donation.id)

    const { width, height } = donation.donationObject

    const { b64, donation_coords } = await mergeDonationImage(wall_img_name, `/tmp/${donation.id}`, { width, height }, config)
    const base64Data = b64.replace(/^data:image\/png;base64,/, "")

    require("fs").writeFileSync(`/tmp/${wall_img_name}`, base64Data, 'base64')

    // for each pending donation, update its document 
    // setting donation_map object with img_x, img_y, img_width and img_height
    const donation_map = {
      img_x: donation_coords.x,
      img_y: donation_coords.y,
      img_width: width,
      img_height: height,
    }
    config.donations_map.push(donation_map)
    await setDonationMap(donation.id, donation_map)
  }
}