const mergeImages = require('merge-images')
const { Canvas, Image } = require('canvas')


async function mergeDonationImage (wall_path, donation_img_path, { width, height }, config) {
    const donation_coords = _getDonationCoords(config, { width, height })
    donation_coords.x > 0 ? donation_coords.x-- : ''
    donation_coords.y > 0 ? donation_coords.y-- : ''
    try {
        const b64 = await mergeImages(
        [
            { 
                src: `/tmp/${wall_path}`, 
                x: 0, 
                y: 0 
            }, 
            {
                src: donation_img_path,
                x: donation_coords.x,
                y: donation_coords.y,
            }
        ], 
        {
            Canvas: Canvas,
            Image: Image,
        })
        
        return { b64, donation_coords }
    } catch (error) {
        throw error
    }
}

module.exports = mergeDonationImage

// helpers / utils

function _getDonationCoords({ max_width, donations_map }, { width, height }) {
    let coord = { x: 0, y: 0 }

    if (donations_map.length == 0) {
        return coord
    }

    function isAvailableCoord(try_coord) {
        // first check if there is enought space to fit the image
        if ((try_coord.x + width) > max_width) return false

        // find intersected rectangles
        let donations = donations_map.filter(donation => {
            const { img_x, img_width, img_y, img_height } = donation
            return rectanglesIntersect(
                try_coord.x, try_coord.y, try_coord.x + width, try_coord.y + height,
                img_x, img_y, img_x + img_width, img_y + img_height
            )
        })

        // if there is a donation within, this coord is not available
        if (donations.length) return false

        return true
    }

    let pixel = 0
    while (!isAvailableCoord(coord)) {
        coord = {
            x: pixel % max_width,
            y: Math.floor(Number(pixel / max_width)),
        }

        pixel++
    }

    return coord
}

function rectanglesIntersect( 
    minAx, minAy, maxAx, maxAy,
    minBx, minBy, maxBx, maxBy) {
    const aLeftOfB = maxAx < minBx;
    const aRightOfB = minAx > maxBx;
    const aAboveB = minAy > maxBy;
    const aBelowB = maxAy < minBy;

    return !( aLeftOfB || aRightOfB || aAboveB || aBelowB );
}
