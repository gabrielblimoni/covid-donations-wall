module.exports = async function (done_donations) {
  const config = require('./base_config')()
  if (done_donations.length == 0) return config

  for (const donation of done_donations) {
    config.donations_map.push(donation.donation_map)
  }

  return config
}