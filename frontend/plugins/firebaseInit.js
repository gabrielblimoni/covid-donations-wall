import Vue from 'vue'
import * as firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'
import 'firebase/analytics'
import firebaseConfig from '../firebase.config'

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}

Vue.prototype.$auth = firebase.auth
Vue.prototype.$firestore = firebase.firestore
Vue.prototype.$storage = firebase.storage
Vue.prototype.$analytics = firebase.analytics
