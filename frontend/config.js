const paypalClientID = process.env.NODE_ENV === 'development'
  ? 'AZi4Rj2Exd3cZQH_WlWqPpRi7-YBdCxAQg0LeRPyQnK2yX14yUbbL83V3tCOd-HFAoS4HIl9iOI8KhD4'
  : 'ATfRtQTigouKWIhhWjGGU4icqCRNHet42-3Ahy_tbw60V78DjPh9cksMqAWmbWPu505qxVAv2B8GGZfh'

export default {
  pixelPriceInCents: 0.10,
  currency: 'BRL',
  paypalClientID
}
