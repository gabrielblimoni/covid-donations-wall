export const state = () => ({
  opennedDonationForm: false,
  donationObject: null,
  paymentDetails: null
})

export const mutations = {
  openDonationForm (state) {
    state.opennedDonationForm = true
  },
  closeDonationForm (state) {
    state.opennedDonationForm = false
  },
  setDonationObject (state, object) {
    state.donationObject = object
  },
  setPaymentDetails (state, details) {
    state.paymentDetails = details
  }
}
